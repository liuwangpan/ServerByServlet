package dao;

/**
 * 所有Operator的公共接口
 * Created by jcala on 2016/3/15.
 */
public interface ConcreteOperatorInter {
    /**
     * @return 根据操作状态生成的最终状态码
     */
    int operate();
}
