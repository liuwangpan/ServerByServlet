package dao;

import model.CheckBean;


/**
 * 用户登录的数据库操作类
 * Created by jcala on 2016/3/15.
 */
public class LogInOperator implements ConcreteOperatorInter {
    private CheckBean bean;

    public LogInOperator(CheckBean bean) {
        this.bean=bean;
    }

    private LogInOperator() {
    }

    @Override
    public int operate() {
        UpdateQuery uq=new UpdateQuery();
        int num=uq.getNum("select count(*) from user where username=?",bean.getUsername());
        if(num==111){
            int update=uq.update("update user set status=1,device=?where username=?",bean.getDevice(),bean.getUsername());
            if(update==1){
                return 111;
            }else{
                return 110;
            }
        }
        else if(num==0){
            return 112;
        }else{
            return 110;
        }
    }
}
