package dao;

import model.CheckBean;

/**
 * 登出时的具体数据库操作类，并生成要返回的后端处理状态码
 * Created by jcala on 2016/3/15.
 */
public class LogOutOperator implements ConcreteOperatorInter {
    private CheckBean bean;
    public LogOutOperator(CheckBean bean){this();this.bean=bean;}
    private LogOutOperator(){}
    /**
     */
    @Override
    public int operate() {
       String sql="update user set device='',set status=0 where username=?";
       new UpdateQuery().update(sql,bean.getUsername());
        return 0;
    }
}
