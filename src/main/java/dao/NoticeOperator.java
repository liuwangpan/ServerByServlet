package dao;

import model.CheckBean;
import model.NoticeBean;

import java.util.List;

/**查询公告具体数据库操作类，并生成要返回的后端处理状态码
 * Created by jcala on 2016/3/16.
 */
public class NoticeOperator implements ConcreteOperatorInter {
    private CheckBean bean;
    private UpdateQuery uq=null;
    private List<NoticeBean> lists=null;
    public NoticeOperator(CheckBean bean) {
        this();
        this.bean=bean;
        uq=new UpdateQuery();
    }
    /**
     * @return 根据操作状态生成的最终状态码
     */
    private NoticeOperator() {
    }
    @Override
    public int operate() {
        String community=getCommunity();
        if(community!=null){
            lists=getNotices(community);
            if(lists!=null&&lists.size()>0){
                return 131;
            }
        }
        return 130;
    }

    /**
     * 根据用户名查询数据库得到用户所在社区名
     * @return
     */
    public String getCommunity(){
      String sql="select community from user where username=?";
        String community=null;
        try {
            community=uq.getforvalue(sql,4,bean.getUsername());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return community;
    }

    /**
     *根据社区名查询数据库得到所有公告
     * @param community 用户所在社区名
     * @return 社区公告的javabean的集合
     */
    public List<NoticeBean> getNotices(String community){
        String sql="select * from notice where commnity=?";
        List<NoticeBean> notices=null;
        try {
            Class claz=Class.forName("model.NoticeBean");
            notices=uq.getForList(claz,sql,community);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  notices;
    }

    public List<NoticeBean> getLists() {
        return lists;
    }
}
