package dao;

import model.UserBean;

/**
 * 注册时具体数据库操作类，并生成要返回的后端处理状态码
 * Created by jcala on 2016/3/15.
 */
public class RegisterOperator implements ConcreteOperatorInter {
    private UserBean user;

    public RegisterOperator(UserBean user) {
        this();
        this.user = user;
    }

    private RegisterOperator() {
    }
    /**
     * @return 根据操作状态生成的最终状态码
     */
    @Override
    public int operate() {
        String sql="insert into user set username=?,password=?,number=?,community=?";
        int i=new UpdateQuery().update(sql, user.getUsername(),
                user.getPassword(), user.getNumber(), user.getCommunity());
        if(i==1){return 101;}
        else{return 100;}
    }
}
