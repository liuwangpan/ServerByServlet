package dao;

import model.CheckBean;

/**安全校验具体数据库操作类，并生成要返回的后端处理状态码
 * Created by jcala on 2016/3/15.
 */
public class SecurityOperator implements ConcreteOperatorInter {
    private CheckBean bean;

    public SecurityOperator(CheckBean bean) {
        this();
        this.bean=bean;
    }

    private SecurityOperator() {
    }
    /**
     * 进行一次安全校验，因为HTTP为短连接，无法判断是APP中用户请求
     * 校验方式：当用户登陆时，传过来设备信息，将用户名与设备信息匹配，同时将表示在线状态的status由0设置为1
     * 校验方式：过滤器会判断传过来的用户名是否与设备信息匹配，status状态是否为1，是的话继续传递相应servlet
     * @return 根据操作状态生成的最终状态码
     */
    @Override
    public int operate() {
        String sql="select count(*) from user where username=? and device=?";
        UpdateQuery uq=new UpdateQuery();
        int i=uq.getNum(sql,bean.getUsername(),bean.getDevice());
        if(i>0){return 1;}
        else{return 0;}
    }
}
