package io.json;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import model.*;

/**
 * 由字符串读取数据到相应的javabean中，通过Gson实现
 * Created by jcala on 2016/3/14.
 */
public class JsonReader {
    public static <T> T read(String jsonString, Class<T> claz) {
        T t = null;
        try {
            Gson gson = new Gson();
            t = gson.fromJson(jsonString, claz);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return t;
    }
   public static int readSate(String jsonString){
       int i=0;
       StateBean sb= null;
       try {
           Class claz = Class.forName("model.StateBean");
           sb = (StateBean)read(jsonString,claz);
       } catch (ClassNotFoundException e) {
           e.printStackTrace();
       }
       return sb.getCode();
   }
    public static UserBean readUser(String jsonString){
       UserBean user=null;
        try {
            Class claz = Class.forName("model.UserBean");
            user=(UserBean)read(jsonString,claz);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }
    public static CheckBean readCheck(String jsonString){
        CheckBean bean=null;
        try {
            Class claz=Class.forName("model.CheckBean");
            bean=(CheckBean)read(jsonString,claz);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return bean;
    }
    public static PmBean readPm(String jsonString){
        PmBean bean=null;
        try {
            Class claz=Class.forName("model.PmBean");
            bean=(PmBean)read(jsonString,claz);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return bean;
    }
    public static CityBean readCity(String jsonString){
        CityBean bean=null;
        try {
            Class claz=Class.forName("model.CityBean");
            bean=(CityBean)read(jsonString,claz);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return bean;
    }
    public static NumberBean readNumber(String jsonString){
        NumberBean bean=null;
        try {
            Class claz=Class.forName("model.NumberBean");
            bean=(NumberBean)read(jsonString,claz);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return bean;
    }
}
