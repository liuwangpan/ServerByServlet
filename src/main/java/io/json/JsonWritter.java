package io.json;

import com.google.gson.Gson;
import model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 将javabean中的数据信息转化为json字符串，通过Gson实现
 * Created by jcala on 2016/3/15.
 */
public class JsonWritter {
    public static String writeToState(StateBean code){
        return (new Gson().toJson(code));
    }
    public static String writeToNotices(NoticesBean tf){
        return (new Gson().toJson(tf));
    }
    public static String WriteToPm25(PmBean pm){
        return (new Gson().toJson(pm));
    }
    public static String WriteToIdentify(IdentifyBean pm){
        return (new Gson().toJson(pm));
    }
    public static String WriteToStatesData(StatesBean sb){
        return (new Gson().toJson(sb));
    }
}
