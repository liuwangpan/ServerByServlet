package logic.handler;

import dao.SecurityOperator;
import model.CheckBean;

/**
 * 安全校验的主要操作类
 * Created by jcala on 2016/3/15.
 */
public class SecurityHandler implements SecurityHandlerInter {
    /**
     *将传入的CheckBean进行安全校验，并返回校验结果的布尔值
     */
    @Override
    public boolean cheack(CheckBean bean) {
        int result=new SecurityOperator(bean).operate();
        if(result<1){return false;}
        return true;
    }
}
