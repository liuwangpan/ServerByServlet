package logic.handler.face.impl;

import dao.ConcreteOperatorInter;
import dao.LogInOperator;
import io.http.HTTPHandler;
import io.json.JsonReader;
import io.json.JsonWritter;
import logic.handler.face.inter.LogInHandlerInter;
import model.CheckBean;
import model.StateBean;

import javax.servlet.http.HttpServletResponse;

/**
 * 用户登录操作的handler
 * Created by jcala on 2016/3/15.
 */
public class LogInHandler implements LogInHandlerInter {
    private String data;
    private HttpServletResponse response;
    public LogInHandler(String data,HttpServletResponse response){
        this();
        this.response=response;
      this.data=data;
    }
    private LogInHandler(){}

    /**
     * 1 获取包含验证信息的CheckBean
     * 2 传入CheckBean到operator中进行更新操作，将用户的设备更
     */
    @Override
    public void handle(){
       StateBean bean=new StateBean(logIn());
        HTTPHandler.sendMsgToHTTP(response, JsonWritter.writeToState(bean));
    }

    /**
     * @return 登陆操作的状态码，111表示登陆成功，110表示c出现异常，112表示用户名密码不匹配，无法完成登陆
     */
    @Override
    public int logIn() {
        CheckBean bean= JsonReader.readCheck(data);
        ConcreteOperatorInter operator=new LogInOperator(bean);
        return operator.operate();
    }
}

