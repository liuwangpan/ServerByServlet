package logic.handler.face.impl;

import dao.LogOutOperator;
import io.json.JsonReader;
import logic.handler.face.inter.LogOutHandlerInter;
import model.CheckBean;

import javax.servlet.http.HttpServletResponse;

/**
 * 登出操作
 * Created by jcala on 2016/3/15.
 */
public class LogOutHandler implements LogOutHandlerInter {
   private String data;
    private HttpServletResponse response;
    public LogOutHandler(String data,HttpServletResponse response){
        this();
        this.response=response;
        this.data=data;
    }
    private LogOutHandler(){}

    /**
     * 1 从HTTP请求解析得到的json数据中解析得到CheckBean
     * 2 借助LogOutOperator类实现登出的数据库操作
     */
    @Override
    public void handle(){
        CheckBean bean= JsonReader.readCheck(data);
        new LogOutOperator(bean).operate();
    }
}
