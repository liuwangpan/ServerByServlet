package logic.handler.face.impl;

import dao.NoticeOperator;
import io.http.HTTPHandler;
import io.json.JsonReader;
import io.json.JsonWritter;
import logic.handler.face.inter.NoticeHandlerInter;
import model.CheckBean;
import model.NoticeBean;
import model.NoticesBean;
import model.StateBean;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 查询公告信息的Handler
 * Created by jcala on 2016/3/16.
 */
public class NoticeHandler implements NoticeHandlerInter {
    private String data;
    private HttpServletResponse response;
    private NoticeOperator operator=null;
    public NoticeHandler(String data,HttpServletResponse response){
        this();
        this.response=response;
        this.data=data;
    }
    private NoticeHandler(){
    }

    /**
     * 1 从HTTP请求解析得到的json数据中解析得到CheckBean
     * 2 借助NoticeOperator查询得到公告信息
     * 3 将封装公告数据和状态码的NoticesBean转为json字符串，再返回给客户端
     */
    @Override
    public void handle() {
        CheckBean bean= JsonReader.readCheck(data);
        operator=new NoticeOperator(bean);
        int result=operator.operate();
        String back=JsonWritter.writeToState(new StateBean(result));
        if(result==131){
            back= JsonWritter.writeToNotices(getNotices());
        }
        HTTPHandler.sendMsgToHTTP(response,back);
    }

    /**
     *
     * @return 要返回给客户端的封装公告信息及状态码信息的javabean
     */
    @Override
    public NoticesBean getNotices() {
        List<NoticeBean> lists=operator.getLists();
        NoticesBean notices=new NoticesBean();
        notices.setCode(131);
        notices.setObjs(lists);
        return notices;
    }
}
