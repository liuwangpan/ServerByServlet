package logic.handler.face.impl;

import dao.ConcreteOperatorInter;
import dao.RegisterOperator;
import io.http.HTTPHandler;
import io.json.JsonReader;
import io.json.JsonWritter;
import logic.handler.face.inter.RegisterHandlerInter;
import model.StateBean;
import model.UserBean;

import javax.servlet.http.HttpServletResponse;

/**
 * 注册操作处理的Handler
 * Created by jcala on 2016/3/15.
 */
public class RegisterHandler implements RegisterHandlerInter {
    private String data;
    private HttpServletResponse response;
    private ConcreteOperatorInter operator=null;
    public RegisterHandler(String data,HttpServletResponse response){
        this();
        this.response=response;
        this.data=data;
    }
    private RegisterHandler(){}

    /**
     * 1 从HTTP中得到的json数据解析得到封装用户信息的UserBean
     * 2 RegisterOperator完成注册的数据库操作
     * 3 返回包含注册结果的StateBean，如果成功状态码值为101，注册异常为100
     */
    @Override
    public void handle(){
         UserBean user=JsonReader.readUser(data);
         int result=login(user);
        HTTPHandler.sendMsgToHTTP(response,JsonWritter.writeToState(new StateBean(result)));
    }
    @Override
    public int login(UserBean user) {
        operator=new RegisterOperator(user);
        int i=operator.operate();
        return i;
    }
}