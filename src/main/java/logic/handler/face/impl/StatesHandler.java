package logic.handler.face.impl;

import dao.StatesOperator;
import io.http.HTTPHandler;
import io.json.JsonReader;
import io.json.JsonWritter;
import logic.handler.face.inter.StatesHandlerInter;
import model.CheckBean;
import model.StateBean;
import model.StatesBean;

import javax.servlet.http.HttpServletResponse;

/**
 * 查询状态信息操作，包括物业状态，维修状态
 * Created by jcala on 2016/3/16.
 */
public class StatesHandler implements StatesHandlerInter {
    private String data;
    private HttpServletResponse response;
    private StatesOperator operator=null;
    public StatesHandler(String data,HttpServletResponse response){
        this();
        this.response=response;
        this.data=data;
    }
    private StatesHandler(){
    }

    /**
     * 1 从HTTP中得到的json字符串解析为CheckBean
     * 2 StatesOperator查询数据库，得到封装好物业信息，维修信息和状态码的StatesBean
     * 3 将封装好物业信息，维修信息和状态码的StatesBean转换为json字符串并返回给客户端
     */
    @Override
    public void handle() {
        CheckBean bean= JsonReader.readCheck(data);
        operator=new StatesOperator(bean);
        int result=operator.operate();
        String back= null;
        if (result==151){
            JsonWritter.writeToState(new StateBean(result));
        }else{
            JsonWritter.WriteToStatesData(getStates());
        }
        HTTPHandler.sendMsgToHTTP(response, back);
    }

    @Override
    public StatesBean getStates() {
        StatesBean states=operator.getAllData();
        states.setCode(151);
        return states;
    }
}
