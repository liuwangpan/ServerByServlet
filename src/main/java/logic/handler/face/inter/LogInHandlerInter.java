package logic.handler.face.inter;

/**
 * Created by jcala on 2016/3/15.
 */
public interface LogInHandlerInter  extends HandlerInter {
    int logIn();
}
