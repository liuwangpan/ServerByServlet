package logic.handler.face.inter;

import model.NoticesBean;

/**
 * Created by jcala on 2016/3/16.
 */
public interface NoticeHandlerInter extends HandlerInter{
    NoticesBean getNotices();
}
