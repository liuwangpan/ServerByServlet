package logic.handler.face.inter;

import model.UserBean;

/**
 * Created by jcala on 2016/3/15.
 */
public interface RegisterHandlerInter extends HandlerInter {
    int login(UserBean user);
}
