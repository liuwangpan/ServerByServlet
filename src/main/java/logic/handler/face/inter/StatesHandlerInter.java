package logic.handler.face.inter;

import model.StatesBean;

/**
 * Created by jcala on 2016/3/16.
 */
public interface StatesHandlerInter extends HandlerInter {
    StatesBean getStates();
}
