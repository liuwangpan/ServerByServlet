package logic.handler.face.inter;

import model.PmBean;

/**
 * Created by jcala on 2016/3/15.
 */
public interface WeatherHandlerInter extends HandlerInter{
   PmBean getWeather();
}
