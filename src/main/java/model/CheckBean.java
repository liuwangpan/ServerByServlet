package model;

/**
 * 封装校验信息的javabean，包括用户名和设备信息
 * Created by jcala on 2016/3/15.
 */
public class CheckBean {
    private String username;
    private String device;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public CheckBean(String username, String device) {
        this();
        this.username = username;
        this.device = device;
    }

    public CheckBean() {
    }
}
