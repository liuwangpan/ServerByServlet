package model;

/**
 * 封装城市信息的javabean，包括城市名和状态码
 * 要返回给客户端的javabean需要包含状态码信息
 * Created by jcala on 2016/3/16.
 */
public class CityBean {
    private int code;
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
