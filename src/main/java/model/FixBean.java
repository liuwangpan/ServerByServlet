package model;

import java.util.Date;

/**
 * 封装维修信息的javabean，包括维修信息，维修日期，工号姓名信息
 * Created by jcala on 2016/3/16.
 */
public class FixBean {
    private String type;
    private Date date;
    private String emid;
    private String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEmid() {
        return emid;
    }

    public void setEmid(String emid) {
        this.emid = emid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
