package model;

/**
 * 封装手机验证码信息的javabean，包括状态码和四位验证码信息
 * 要返回给客户端的javabean需要包含状态码信息
 * Created by jcala on 2016/3/16.
 */
public class IdentifyBean {
    private int code;
    private int random;

    public int getRandom() {
        return random;
    }

    public void setRandom(int random) {
        this.random = random;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
