package model;

/**
 * 封装一条公告信息的的javabean，包括社区名，公共标题和具体公告内容
 * Created by jcala on 2016/3/15.
 */
public class NoticeBean {
    private String community;
    private String content;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NoticeBean(String community, String content) {
        this();
        this.community = community;
        this.content = content;
    }

    public NoticeBean() {
    }


}
