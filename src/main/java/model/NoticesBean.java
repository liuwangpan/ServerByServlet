package model;


import java.util.List;

/**
 * 封装多条公告信息的的javabean，包括要返回的所有公告信息和状态码
 * 要返回给客户端的javabean需要包含状态码信息
 * Created by jcala on 2016/3/15.
 */
public class NoticesBean {
    private int code;
    private List<NoticeBean> objs;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<NoticeBean> getObjs() {
        return objs;
    }

    public void setObjs(List<NoticeBean> objs) {
        this.objs = objs;
    }

    public NoticesBean(int code, List<NoticeBean> objs) {
        this();
        this.code = code;
        this.objs = objs;
    }

    public NoticesBean() {
    }
}
