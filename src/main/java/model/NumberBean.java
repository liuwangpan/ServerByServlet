package model;

/**
 * 封装包含手机号信息的javabean
 * Created by jcala on 2016/3/16.
 */
public class NumberBean {
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
