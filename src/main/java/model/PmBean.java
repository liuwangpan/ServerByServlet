package model;

/**
 * 封装pm2.5信息的javabean，包括pm2.5的值
 * 要返回给客户端的javabean需要包含状态码信息
 * Created by jcala on 2016/3/16.
 */
public class PmBean {
    private int pm25;
    public int getPm25() {
        return pm25;
    }

    public void setPm25(int pm25) {
        this.pm25 = pm25;
    }


    public PmBean() {
    }

    @Override
    public String toString() {
        return "PmBean{" +
                "pm25=" + pm25 +
                '}';
    }
}
