package model;

import java.util.Date;

/**
 * 封装物业费信息的javabean，包括上次缴费日期和下次要缴费日期
 * 要返回给客户端的javabean需要包含状态码信息
 * Created by jcala on 2016/3/16.
 */
public class PropertyBean {
    private Date paid;
    private Date next;

    public Date getNext() {
        return next;
    }

    public void setNext(Date next) {
        this.next = next;
    }

    public Date getPaid() {
        return paid;
    }

    public void setPaid(Date paid) {
        this.paid = paid;
    }
}
