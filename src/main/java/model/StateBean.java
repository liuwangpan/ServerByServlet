package model;

/**
 * 封装请求码信息的javabean，包括请求码信息
 * Created by jcala on 2016/3/15.
 */
public class StateBean {
    private int code;

    public StateBean(int code) {
        this.code = code;
    }

    public StateBean() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "StateBean{" +
                "code=" + code +
                '}';
    }
}
