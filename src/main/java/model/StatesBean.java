package model;

import java.util.List;

/**
 * 要返回给客户端的javabean需要包含状态码信息
 * Created by jcala on 2016/3/16.
 */
public class StatesBean{
    private int code;
    private PropertyBean property;
    private List<FixBean> fixes;

    public PropertyBean getProperty() {
        return property;
    }

    public void setProperty(PropertyBean property) {
        this.property = property;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<FixBean> getFixes() {
        return fixes;
    }

    public void setFixes(List<FixBean> fixes) {
        this.fixes = fixes;
    }

    public StatesBean(PropertyBean property, List<FixBean> fixes) {
        this();
        this.property = property;
        this.fixes = fixes;
    }

    public StatesBean() {
    }
}
