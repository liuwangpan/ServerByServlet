package model;


/**
 *封装用户信息的javabean，依次为用户名，密码，手机号，社区名，是否在线，维修id，物业id，设备信息
 * Created by jcala on 2016/3/14.
 */
public class UserBean {
    private String username;
    private String password;
    private String number;
    private String community;
    private boolean onLine;
    private int fixid;
    private int propertyid;
    private String device;

    public int getFixid() {
        return fixid;
    }

    public void setFixid(int fixid) {
        this.fixid = fixid;
    }

    public int getPropertyid() {
        return propertyid;
    }

    public void setPropertyid(int propertyid) {
        this.propertyid = propertyid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public boolean isOnLine() {
        return onLine;
    }

    public void setOnLine(boolean onLine) {
        this.onLine = onLine;
    }


    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

}
