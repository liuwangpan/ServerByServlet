package model;

/**
 * 要返回给客户端的javabean需要包含状态码信息
 * Created by jcala on 2016/3/15.
 */
public class WeatherBean {
    private int code;
    private String aqi;
    private String quality;
    private String pm;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getAqi() {
        return aqi;
    }

    public void setAqi(String aqi) {
        this.aqi = aqi;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getPm() {
        return pm;
    }

    public void setPm(String pm) {
        this.pm = pm;
    }
}
