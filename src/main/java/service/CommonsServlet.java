package service;

import io.http.HTTPHandler;
import io.json.JsonReader;
import logic.handler.face.impl.WeatherHandler;
import model.CityBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 普通信息的操作类，包括天气信息查询
 * Created by jcala on 2016/3/15.
 */
@WebServlet(name = "CommonsServlet",urlPatterns = "/commons.action")
public class CommonsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     doPost(request,response);
    }

    /**
     *从HTTP信息中解析请求码，如果请求码为104，WeatherHandler将进行查询pm2.5操作并返回封装好pm2.5信息的javabean
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonData= HTTPHandler.receiveFromHTTP(request);
        int state= JsonReader.readSate(jsonData);
        if(state==104){
            CityBean bean=JsonReader.readCity(jsonData);
            new WeatherHandler(bean,response);
        }
        else{}
    }
}
