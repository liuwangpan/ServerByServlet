package service;

import logic.handler.IdentifyCodeHandler;
import logic.handler.face.impl.LogInHandler;
import logic.handler.face.impl.LogOutHandler;
import logic.handler.face.impl.RegisterHandler;
import io.http.HTTPHandler;
import io.json.JsonReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *用户注册，登录，登出操作的Servlet
 * Created by jcala on 2016/3/14.
 */
@WebServlet(name = "LoginRegisterServlet", urlPatterns = "/loginRegister")
public class LoginRegisterServlet extends HttpServlet {
    /**
     *首先从HTTP解析请求信息得到请求码，根据请求码的值判断进行注册操作，登录操作还是登出操作
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonData = HTTPHandler.receiveFromHTTP(request);
        int state = JsonReader.readSate(jsonData);
        if (state == 100) {
            new RegisterHandler(jsonData, response).handle();
        } else if (state == 101) {
            new LogInHandler(jsonData, response).handle();
        } else if (state == 103) {
            new LogOutHandler(jsonData, response).handle();
        } else if (state==99) {
            new IdentifyCodeHandler(jsonData,response).handle();
        } else {//什么也不做
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
