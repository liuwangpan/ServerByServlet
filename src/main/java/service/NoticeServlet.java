package service;

import io.http.HTTPHandler;
import io.json.JsonReader;
import logic.handler.face.impl.NoticeHandler;
import logic.handler.face.impl.WeatherHandler;
import model.CityBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 公告信息查询的Servlet
 * Created by jcala on 2016/3/16.
 */
@WebServlet(name = "NoticeServlet", urlPatterns = "/notice.action")
public class NoticeServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonData= HTTPHandler.receiveFromHTTP(request);
        int state= JsonReader.readSate(jsonData);
        if(state==102){
            new NoticeHandler(jsonData,response).handle();
        }
        else{}
    }
}
