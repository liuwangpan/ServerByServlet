package service;

import io.http.HTTPHandler;
import io.json.JsonReader;
import logic.handler.face.impl.NoticeHandler;
import logic.handler.face.impl.StatesHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 查询状态信息的servlet，包括维修状态查询和物业费查询
 * Created by jcala on 2016/3/16.
 */
@WebServlet(name = "StatesServlet",urlPatterns = "/states.action")
public class StatesServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      doPost(request,response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonData= HTTPHandler.receiveFromHTTP(request);
        int state= JsonReader.readSate(jsonData);
        if(state==105){
           new StatesHandler(jsonData,response).handle();
        }
        else{}
    }
}
