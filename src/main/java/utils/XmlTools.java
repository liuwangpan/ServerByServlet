package utils;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

/**
 * 解析xml字符串，获取手机验证码是否发送成功的状态信息
 * Created by jcala on 2016/3/16.
 */
public class XmlTools {
    public boolean identifyCodeStatus(String SubmitResult){
        Document doc = null;
        String code ="";
        try {
            doc=DocumentHelper.parseText(SubmitResult);
            Element root = doc.getRootElement();
            code=root.elementText("code");
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        if("2".equals(code)){
           return true;
        }
        return false;
    }
}
